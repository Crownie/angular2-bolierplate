var gulp = require('gulp'),
  browserSync = require('browser-sync'),
  reload = browserSync.reload;
typescript = require('gulp-typescript'),
  sourcemaps = require('gulp-sourcemaps'),
  sass = require('gulp-sass'),
  tscConfig = require('./tsconfig.json');

var appSrc = 'builds/development/',
  pSrc = 'process/';

gulp.task('html', function () {
  gulp.src(appSrc + '**/*.html');
});

gulp.task('sass', function () {
  return gulp.src(pSrc + 'sass/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest(appSrc + '/css'));
});

gulp.task('copylibs', function () {
  return gulp
    .src([
      'node_modules/es6-shim/es6-shim.min.js',
      'node_modules/systemjs/dist/system-polyfills.js',
      'node_modules/angular2/bundles/angular2-polyfills.js',
      'node_modules/systemjs/dist/system.src.js',
      'node_modules/rxjs/bundles/Rx.js',
      'node_modules/angular2/bundles/angular2.dev.js'
    ])
    .pipe(gulp.dest(appSrc + 'js/lib/angular2'));
});

gulp.task('typescript', function () {
  return gulp
    .src(pSrc + 'typescript/**/*.ts')
    .pipe(sourcemaps.init())
    .pipe(typescript(tscConfig.compilerOptions))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(appSrc + 'js/'));
});

gulp.task('watch', function () {
  gulp.watch(pSrc + 'typescript/**/*.ts', ['typescript']);
  gulp.watch(pSrc + 'sass/**/*.scss', ['sass']);

  gulp.watch([
    appSrc+'**/*'
  ]).on('change', reload);
});

gulp.task('webserver', function () {
  browserSync({
    notify: false,
    port: 9000,
    server: {
      baseDir: [appSrc]
    }
  });
  
});

gulp.task('default', ['copylibs', 'typescript', 'sass', 'watch', 'webserver']);
