import {Component} from 'angular2/core';

@Component({
    selector: 'my-app',
    templateUrl: 'partials/app.html',
})
export class AppComponent {
    dateString = new Date().toDateString();
}